# RISC-V 64 Shit OS
![Screenshot of OS](screenshot.png "Now with multitasking!")

# Building

Just run `make`. That's it.

# Running in QEMU

I am 99% sure this will not run outside of QEMU, so do `make run` to run.

# Notes

This project does not contain C++, it's a bug in Gitlab.

# License
2 clause BSD, found in LICENSE
