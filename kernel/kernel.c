/* kernel.c - Base kernel */

#include <stdint.h>
#include <stddef.h>
#include <stdarg.h>
#include "headers/uart.h"
#include "headers/print.h"
#include "headers/util.h"
#include "headers/reg.h"
#include "headers/delay.h"
#include "headers/tasks.h"
#include "headers/prompt.h"

char* itoareg;

/* Init process - for now, just switch task to prompt */
void init() {
	puts("init: loading prompt\r\n");
	pid_t pid = t_create(prompt);
	t_switch(pid);
}

/* Start of execution */
int kmain(void) { 
	puts("Welcome to riscv64-shit-os version ");
	puts(VERSION);
	puts("!\r\n");
	puts("Starting init\r\n");
	pid_t pid = t_create(init);
	t_switch(pid);
	return 0;
}
