#ifndef SWITCH_H
#define SWITCH_H

#include "reg.h"
extern void timer();
extern void swtch(struct context *ctx_old, struct context *ctx_new);

#endif  
