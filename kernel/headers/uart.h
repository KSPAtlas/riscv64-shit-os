/* uart.h - UART Definitions, based off mini-riscv-os */

#ifndef UART_H
#define UART_H

#define UART 0x10000000 /* UART base address */ 
#define UART_THR (uint8_t*)(UART+0x00) /* Transmitter Holding Register */
#define UART_LSR (uint8_t*)(UART+0x05) /* Line Status Register */
#define UART_LSR_EMPTY_MASK 0x40

#endif
