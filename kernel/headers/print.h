/* print.h - Functions for printing to UART - based off mini-riscv-os */

#ifndef PRINT_H
#define PRINT_H

int putc(char ch) {
	while((*UART_LSR & UART_LSR_EMPTY_MASK) == 0);
	return *UART_THR = ch;
}

void puts(char *s) {
	while(*s) putc(*s++);
}

char getc(void) {	
	return *((char*)UART);
}

#endif
