/* util.h - General utilities */
#ifndef UTIL_H
#define UTIL_H

int strcmp(const char *a, const char *b){
	while (*a && *a == *b) { ++a; ++b; }
	return (int)(unsigned char)(*a) - (int)(unsigned char)(*b);
}

#endif
