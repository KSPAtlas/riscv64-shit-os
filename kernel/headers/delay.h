#ifndef DELAY_H
#define DELAY_H

void delay(volatile int count) {
	count *= 50000;
	while(count--);
}

#endif
