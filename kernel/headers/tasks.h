#ifndef TASKS_H
#define TASKS_H

typedef int pid_t; /* Make a type for PIDs */

#define STACK_SIZE 1024
#define TASK_LIMIT 15

uint8_t task_stack[TASK_LIMIT][STACK_SIZE];
struct context ctx_os;
struct context ctx_tasks[TASK_LIMIT];
struct context *ctx_focused;
pid_t tasks = 0;

extern void swtch();

pid_t t_create(void (*task)(void)) {
 	int pid = tasks++;
	ctx_tasks[pid].ra = (reg_t) task;
	ctx_tasks[pid].sp = (reg_t) &task_stack[pid][STACK_SIZE - 1];
	return pid;
}

void t_switch(pid_t pid) {
	ctx_focused = &ctx_tasks[pid];
	swtch(&ctx_os, ctx_focused);
}

#endif
