/* prompt.h - Provides a Machine Mode prompt for entering basic commands */

#ifndef PROMPT_H
#define PROMPT_H

char store;
char strstore[100];
int charcount = 0;

void parse(char* cmd) {
	/* I know how to use switch(), but C doesn't allow switch() on a string so I'm forced to go YandereDev style */
	if (strcmp(cmd, "info") == 0) {
		puts("SysInfo: Name: riscv64-shit-os\r\n");
	} else if (strcmp(cmd, "ls") == 0) {
		puts("I know it's muscle memory, but no file system is implemented yet.\r\n");
	} else if (strcmp(cmd, "help") == 0) {
		puts("HELP:\r\ninfo: System information\r\nhelp: This.\r\n");
 	} else if (strcmp(cmd, "amogus") == 0) {
		puts("Sussy amogus11!11!\r\n");
 	} else if (strcmp(cmd, "halt") == 0) {
		puts("Halting");
		for(;;) {
			asm("wfi");
		}
	} else {
		puts("Command not found\r\n");
	}
}

/* The prompt */
/* TODO: Fix the first command not being parsed properly */

void prompt() {
	puts("Welcome to riscv64-shit-os S-Mode prompt! Type \"help\" for more info.\r\nsupervisor@riscv-shit-os % ");
	for(;;) {
		store = getc();
		putc(store);
		if(store == '\r') {
			putc('\n');
			charcount = 0;
			parse(strstore);
			for(int i = 0; i < 100; i++) strstore[i] = 0;
			puts("supervisor@riscv-shit-os % ");
		} else {
			strstore[charcount] = store;
			charcount++;
		}
		while(store == getc());
	}
}

#endif
