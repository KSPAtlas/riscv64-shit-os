# Makefile - makefile
CC=riscv64-elf-gcc
CFLAGS=-nostdlib -fno-builtin -mcmodel=medany -march=rv64ima -mabi=lp64 -T linker.ld -Wall -Werror -D VERSION=\"git-$(shell git log -1 --pretty=format:%h)\"

all: kernel.elf

kernel.elf: boot/boot.S kernel/kernel.c kernel/switch.S
	${CC} ${CFLAGS} -o $@ $^

run: kernel.elf
	qemu-system-riscv64 -smp 4 -machine virt -monitor stdio -bios $<

clean:
	rm kernel.elf	
